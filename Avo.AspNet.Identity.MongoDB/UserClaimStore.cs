using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using MongoDB.Driver;

namespace Avo.AspNet.Identity.MongoDB
{
    /// <summary>
    /// Inherits from base UserStore and implements IUserClaimStore. Suitable for use in claims-based identity
    /// </summary>
    public class UserClaimStore<TUser> : 
        UserStore<TUser>, 
        IUserClaimStore<TUser>
        where TUser : IdentityUser
    {
        public UserClaimStore(IMongoCollection<TUser> userCollection) 
            : base(userCollection){}

        public virtual Task<IList<Claim>> GetClaimsAsync(TUser user)
        {
            ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            var list = user.Claims.Select(c => c.ToSecurityClaim()).ToList();
            return Task.FromResult((IList<Claim>)list);
        }

        public virtual Task AddClaimAsync(TUser user, Claim claim)
        {
            ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            if (claim == null)
            {
                throw new ArgumentNullException(nameof(claim));
            }

            user.Claims.Add(new IdentityUserClaim(claim.Type, claim.Value));
            return Task.CompletedTask;
        }

        public virtual Task RemoveClaimAsync(TUser user, Claim claim)
        {
            ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            if (claim == null)
            {
                throw new ArgumentNullException(nameof(claim));
            }

            user.Claims.Remove(new IdentityUserClaim(claim.Type, claim.Value));
            return Task.CompletedTask;
        }

    }
}