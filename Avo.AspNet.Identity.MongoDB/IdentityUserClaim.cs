﻿using System.Security.Claims;

namespace Avo.AspNet.Identity.MongoDB
{
    public class IdentityUserClaim
    {
        public IdentityUserClaim(){}

        public IdentityUserClaim(string claimType, string claimValue)
        {
            Type = claimType;
            Value = claimValue;
        }

        public IdentityUserClaim(Claim claim)
        {
            Type = claim.Type;
            Value = claim.Value;
        }

        public string Type { get; set; }
        public string Value { get; set; }

        public Claim ToSecurityClaim()
        {
            return new Claim(Type, Value);
        }
    }
}