using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using MongoDB.Driver;

namespace Avo.AspNet.Identity.MongoDB
{
    /// <summary>
    /// Inherits from base UserStore and implements IUserRoleStore. Suitable for use in roles-based identity
    /// </summary>
    public class UserRoleStore<TUser> : 
        UserStore<TUser>, 
        IUserRoleStore<TUser>
        where TUser: IdentityUser
    {
        public UserRoleStore(IMongoCollection<TUser> userCollection) 
            : base(userCollection) {}

        public virtual Task AddToRoleAsync(TUser user, string roleName)
        {
            ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            user.Roles.Add(roleName);
            return Task.CompletedTask;
        }

        public virtual Task RemoveFromRoleAsync(TUser user, string roleName)
        {
            ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            user.Roles.Remove(roleName);
            return Task.CompletedTask;
        }

        public virtual Task<IList<string>> GetRolesAsync(TUser user)
        {
            ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            return Task.FromResult((IList<string>)user.Roles);
        }

        public virtual Task<bool> IsInRoleAsync(TUser user, string roleName)
        {
            ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            var isInRole = user.Roles.Contains(roleName);
            return Task.FromResult(isInRole);
        }
    }
}