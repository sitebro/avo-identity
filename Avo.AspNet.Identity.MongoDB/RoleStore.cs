﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using MongoDB.Driver;

namespace Avo.AspNet.Identity.MongoDB
{
    /// <summary>
    /// Suitable if you want to manage roles in your application
    /// </summary>
    public class RoleStore<TRole>
        : IRoleStore<TRole>,IQueryableRoleStore<TRole, string>
        where TRole : IdentityRole
    {
        private readonly IMongoCollection<TRole> _rolesCollection;
        private bool _disposed;

        public RoleStore(IMongoCollection<TRole> rolesCollection)
        {
            _rolesCollection = rolesCollection;
        }

        public Task CreateAsync(TRole role)
        {
            ThrowIfDisposed();
            if (role == null)
            {
                throw new ArgumentNullException(nameof(role));
            }
            return Roles.Any(r => r.Name == role.Name) 
                ? Task.CompletedTask 
                : _rolesCollection.InsertOneAsync(role);
        }

        public Task UpdateAsync(TRole role)
        {
            ThrowIfDisposed();
            if (role == null)
            {
                throw new ArgumentNullException(nameof(role));
            }
            return _rolesCollection.ReplaceOneAsync(r => r.Id == role.Id, role);
        }

        public Task DeleteAsync(TRole role)
        {
            ThrowIfDisposed();
            if (role == null)
            {
                throw new ArgumentNullException(nameof(role));
            }
            return _rolesCollection.DeleteOneAsync(r => r.Id == role.Id);
        }

        public Task<TRole> FindByIdAsync(string roleId)
        {
            ThrowIfDisposed();
            return _rolesCollection.Find(r => r.Id == roleId).FirstOrDefaultAsync();
        }

        public Task<TRole> FindByNameAsync(string roleName)
        {
            ThrowIfDisposed();
            return _rolesCollection.Find(r => r.Name == roleName).FirstOrDefaultAsync();
        }
        protected void ThrowIfDisposed()
        {
            if (_disposed)
            {
                throw new ObjectDisposedException(GetType().Name);
            }
        }
        public void Dispose()
        {
            _disposed = true;
        }

        public IQueryable<TRole> Roles => _rolesCollection.AsQueryable();
    }
}
