﻿using System;
using System.Linq;
using Avo.AspNet.Identity.MongoDB;
using NUnit.Framework;

namespace Avo.AspNet.Identity.Tests
{
    [TestFixture]
	public class IndexChecksTests : UserIntegrationTestsBase
	{
		[Test]
		public void EnsureUniqueIndexOnUserName_NoIndexOnUserName_AddsUniqueIndexOnUserName()
		{
			var userCollectionName = "userindextest";
			Database.DropCollection(userCollectionName);
			var users = Database.GetCollection<IdentityUser>(userCollectionName);

			IndexChecks.EnsureUniqueIndexOnUserName(users);
		    using (var cursor = users.Indexes.List())
		    {
		        while (cursor.MoveNext())
		        {
		            var index = cursor.Current
		            .Where(i => i["unique"] == true)
                    .Where(i => i["key"].AsBsonArray.Count == 1)
		            .First(i => i["key"].AsBsonArray.Contains("UserName"));
		            Expect(index["key"].AsBsonArray.Count, Is.EqualTo(1));
		        }
            }
			
		}

        [Test]
        public void EnsureEmailUniqueIndex_NoIndexOnEmail_AddsUniqueIndexOnEmail()
        {
            var userCollectionName = "userindextest";
            Database.DropCollection(userCollectionName);
            var users = Database.GetCollection<IdentityUser>(userCollectionName);

            IndexChecks.EnsureUniqueIndexOnEmail(users);

            using (var cursor = users.Indexes.List())
            {
                while (cursor.MoveNext())
                {
                    var index = cursor.Current
                        .Where(i => i["unique"] == true)
                        .Where(i => i["key"].AsBsonArray.Count == 1)
                        .First(i => i["key"].AsBsonArray.Contains("Email"));
                    Expect(index["key"].AsBsonArray.Count, Is.EqualTo(1));
                }
            }
        }

        [Test]
        public void EnsureUniqueIndexOnRoleName_NoIndexOnRoleName_AddsUniqueIndexOnRoleName()
        {
            var roleCollectionName = "roleindextest";
            Database.DropCollection(roleCollectionName);
            var roles = Database.GetCollection<IdentityRole>(roleCollectionName);

            IndexChecks.EnsureUniqueIndexOnRoleName(roles);
            
            using (var cursor = roles.Indexes.List())
            {
                while (cursor.MoveNext())
                {
                    var index = cursor.Current
                        .Where(i => i["unique"] == true)
                        .Where(i => i["key"].AsBsonArray.Count == 1)
                        .First(i => i["key"].AsBsonArray.Contains("Name"));
                    Expect(index["key"].AsBsonArray.Count, Is.EqualTo(1));
                }
            }
        }
    }
}